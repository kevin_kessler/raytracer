package de.fhkl.imst.i.cgma.raytracer;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.Vector;

import de.fhkl.imst.i.cgma.raytracer.file.I_Sphere;
import de.fhkl.imst.i.cgma.raytracer.file.RTFile;
import de.fhkl.imst.i.cgma.raytracer.file.RTFileReader;
import de.fhkl.imst.i.cgma.raytracer.file.RT_Object;
import de.fhkl.imst.i.cgma.raytracer.file.T_Mesh;
import de.fhkl.imst.i.cgma.raytracer.gui.IRayTracerImplementation;
import de.fhkl.imst.i.cgma.raytracer.gui.RayTracerGui;
import de.fhkl.imst.i.cgma.raytracer.utils.MyMath;

public class Raytracer implements IRayTracerImplementation {
	public static final Color BACKGROUND = Color.BLACK;
	// viewing volume with infinite end
	private float fovyDegree;
	private float near;
	private float fovyRadians;
	
	// one hardcoded point light as a minimal solution :-(
    private float[] Ia = { 0.25f, 0.25f, 0.25f }; // ambient light color
    private float[] Ids = { 1.0f, 1.0f, 1.0f }; // diffuse and specular light color
    private float[] ICenter = { 4.0f, 4.0f, 2.0f }; // center of point light

	RayTracerGui gui = new RayTracerGui(this);

	private int resx, resy; // viewport resolution
	private float h, w, aspect; // window height, width and aspect ratio

	Vector<RT_Object> objects;

	private Raytracer() {
		try {
		    
		    String directory = System.getProperty("user.dir");
		    gui.addObject(RTFileReader.read(I_Sphere.class, new File(directory+"/data/ikugel2.dat")));
		    gui.addObject(RTFileReader.read(I_Sphere.class, new File(directory+"/data/ikugel.dat")));
		    gui.addObject(RTFileReader.read(T_Mesh.class, new File(directory+"/data/dreieck1.dat")));
		    gui.addObject(RTFileReader.read(T_Mesh.class, new File(directory+"/data/dreieck2.dat")));
		    gui.addObject(RTFileReader.read(T_Mesh.class, new File(directory+"/data/kugel1.dat")));
		    gui.addObject(RTFileReader.read(T_Mesh.class, new File(directory+"/data/kugel2.dat")));
		    gui.addObject(RTFileReader.read(T_Mesh.class, new File(directory+"/data/kugel3.dat")));
		    gui.addObject(RTFileReader.read(T_Mesh.class, new File(directory+"/data/dreiecke1.dat")));
		    gui.addObject(RTFileReader.read(T_Mesh.class, new File(directory+"/data/dreiecke2.dat")));
		    gui.addObject(RTFileReader.read(T_Mesh.class, new File(directory+"/data/dreiecke3.dat")));
		    gui.addObject(RTFileReader.read(T_Mesh.class, new File(directory+"/data/dreiecke4.dat")));
			

		    gui.addObject(RTFileReader.read(I_Sphere.class, new File(directory+"/data/octkugel0.dat")));
		    gui.addObject(RTFileReader.read(I_Sphere.class, new File(directory+"/data/octkugel1.dat")));
		    gui.addObject(RTFileReader.read(I_Sphere.class, new File(directory+"/data/octkugel2.dat")));
		    gui.addObject(RTFileReader.read(I_Sphere.class, new File(directory+"/data/octkugel3.dat")));
		    gui.addObject(RTFileReader.read(I_Sphere.class, new File(directory+"/data/octkugel2-3.dat")));
		    gui.addObject(RTFileReader.read(I_Sphere.class, new File(directory+"/data/octkugel4.dat")));
		    gui.addObject(RTFileReader.read(I_Sphere.class, new File(directory+"/data/octkugel5.dat")));
		    gui.addObject(RTFileReader.read(I_Sphere.class, new File(directory+"/data/octkugel6.dat")));
		    gui.addObject(RTFileReader.read(I_Sphere.class, new File(directory+"/data/octkugel7.dat")));
		    gui.addObject(RTFileReader.read(I_Sphere.class, new File(directory+"/data/octkugel3-7.dat")));

		    objects = gui.getObjects();

		} catch (IOException e) {
		    e.printStackTrace();
		}
	}

	public void setViewParameters(float fovyDegree, float near) {
		// set attributes fovyDegree, fovyRadians, near
		this.fovyDegree = fovyDegree;
		this.fovyRadians = (float) Math.toRadians(fovyDegree);
		this.near = near;

		// set attributes resx, resy, aspect
		resx = gui.getResX();
		resy = gui.getResY();
		aspect = (float)resx / resy;
		

		// set attributes h, w
		h = 2*this.near * (float)Math.tan(this.fovyRadians / 2);
		w = h * aspect;

	}

	@Override
	public void doRayTrace() {
		float x, y, z; // intersection point in viewing plane
		float rayEx, rayEy, rayEz; // eye point==ray starting point
		float rayVx, rayVy, rayVz; // ray vector
		Color color;

		// hardcoded viewing volume with fovy and near
		setViewParameters(90.0f, 1.0f);
		// set eye point
		rayEx = 0.0f;
		rayEy = 0.0f;
		rayEz = 0.0f;

		z = -near;
		
		// prepare mesh data (normals and areas)
		prepareMeshData();
		// prepare mesh data for shading
		precalculateMeshDataShading();

		// xp, yp: pixel coordinates
		for (int xp = 0; xp < resx; ++xp) {
			for (int yp = 0; yp < resy; ++yp) {
				// for demo purposes
				gui.setPixel(xp, yp, BACKGROUND.getRGB());

				// x, y: view coordinates
				x = transformDisplayToViewCoordinateFor_x(xp);
				y = transformDisplayToViewCoordinateFor_y(yp);
				
				// ray vector
				// end point minus start point; (x/y/z) - (rayEx/rayEy/rayEz)
				rayVx = x - rayEx;
				rayVy = y - rayEy;
				rayVz = z - rayEz;
				
				// get color or null along the ray
				color = traceRayAndGetColor(rayEx, rayEy, rayEz, rayVx, rayVy, rayVz);
				// if color!=null set pixel with color
				if(null != color)
					gui.setPixel(xp, yp, color.getRGB());
			}
		}
	}
	
	public float transformDisplayToViewCoordinateFor_x(int xInDisplay) {
		return (float)xInDisplay / (resx-1) * w - w/2;
	}
	public float transformDisplayToViewCoordinateFor_y(int yInDisplay) {
		return (float)(resy-1-yInDisplay) / (resy-1) * h - h/2;
	}


	/** returns Color object or null if no intersection was found */
	private Color traceRayAndGetColor(float rayEx, float rayEy, float rayEz,
			float rayVx, float rayVy, float rayVz) {
		// RTFile scene = gui.getFile();

		double minT = Float.MAX_VALUE;
		int minObjectsIndex = -1;
		int minIndexInT_Mesh = -1;
		float[] minIP = new float[3];
		float[] minN = new float[3];
		float[] minMaterial = new float[3];
		float minMaterialN = 1;
		
		float[] v = new float[3];
		float[] l = new float[3];
		
		float bu = 0.0f; 
		float bv = 0.0f;
		float bw = 0.0f;

		RTFile scene;
		I_Sphere sphere;
		T_Mesh mesh;

		// loop over all scene objects to find the nearest intersection, that
		// is:
		// object with number minObjectIndex
		// minT is the minimal factor t of the ray equation s(t)=rayE+t*rayV
		// where the nearest intersection takes place
		// minMaterial and minMaterialN is the material to use at the nearest
		// intersection point
		for (int objectsNumber = 0; objectsNumber < objects.size(); objectsNumber++) {
			scene = objects.get(objectsNumber);

			// object is an implicit sphere?
			if (scene instanceof I_Sphere) {
				sphere = (I_Sphere) scene;

				float t;
				
				// no bounding box hit? -> next object
				if (!bboxHit(sphere, rayEx, rayEy, rayEz, rayVx, rayVy, rayVz))
				    continue;
				
				// ray intersection uses quadratic equation
				float a, b, c, d;
				
				a = MyMath.scalarProduct(rayVx, rayVy, rayVz, 
						rayVx,  rayVy, rayVz); // v * v
				
				float e_minus_m_x = rayEx - sphere.center[0];
				float e_minus_m_y = rayEy - sphere.center[1];
				float e_minus_m_z = rayEz - sphere.center[2];
				b = MyMath.scalarProduct(2*rayVx, 2*rayVy, 2*rayVz, 
						e_minus_m_x, e_minus_m_y, e_minus_m_z); // 2v * (e-m)
				c = MyMath.scalarProduct(e_minus_m_x, e_minus_m_y, e_minus_m_z,
						e_minus_m_x, e_minus_m_y, e_minus_m_z)
						- sphere.radius * sphere.radius; // (e-m) * (e-m) - r²

				// positive discriminant determines intersection
				d = b*b - 4*a*c;
				// no intersection point? => next object
				if (d <= 0)
					continue;

				// from here: intersection takes place!

				// calculate first intersection point with sphere along the
				// ray
				float sqrt_d = (float) Math.sqrt(d);
				t = (-b + sqrt_d) / (2*a);
				// get smallest t
				if( ! MyMath.almostEqualsToZero(sqrt_d)) {
					float tmp = (-b - sqrt_d) / (2*a);
					if(tmp < t) {
						t = tmp;
					}
				}
				
				// already a closer intersection point? => next object
				if (t >= minT)
					continue;

				// from here: t < minT
				// I'm the winner until now!

				minT = t;
				minObjectsIndex = objectsNumber;
				

				// prepare everything for phong shading
				// the intersection point
				minIP[0] = rayEx + t * rayVx;
				minIP[1] = rayEy + t * rayVy;
				minIP[2] = rayEz + t * rayVz;

				// the normal vector at the intersection point
				// equals vector from sphere center to IP
				minN[0] = minIP[0] - sphere.center[0];
				minN[1] = minIP[1] - sphere.center[1];
				minN[2] = minIP[2] - sphere.center[2];
				
				normalize(minN);				

				// the material
				minMaterial = sphere.material;
				minMaterialN = sphere.materialN;

			}// object is a triangle mesh?
		    else if (scene instanceof T_Mesh) {
		    	mesh = (T_Mesh) scene;

				float t;
				float[] n;
				float[] ip = new float[3];
				
				// no bounding box hit? -> next object
				if (!bboxHit(mesh, rayEx, rayEy, rayEz, rayVx, rayVy, rayVz))
				    continue;

				float a, rayVn, pen;
				float[] p1, p2, p3;
				float[] ai = new float[3];

				// loop over all triangles
				for (int i = 0; i < mesh.triangles.length; i++) {
				    // get the three vertices
				    p1 = mesh.vertices[mesh.triangles[i][0]];
				    p2 = mesh.vertices[mesh.triangles[i][1]];
				    p3 = mesh.vertices[mesh.triangles[i][2]];

				    // fetch precalculated face areas and face normals
				    a = mesh.triangleAreas[i];
				    n =	mesh.triangleNormals[i];		    		

				    rayVn = MyMath.scalarProduct(rayVx, rayVy, rayVz, n[0], n[1], n[2]);

				    // backface? => next triangle
				    if (rayVn >= 0)
				    	continue;	    

				    // no intersection point? => next triangle
				    if (Math.abs(rayVn) < 1E-7)
				    	continue;
				    
				    // pen = (p-e)*n
				    float[] p = p1 ;// p has to be a point in the plane of the triangle
				    pen = MyMath.scalarProduct(p[0]- rayEx, p[1]- rayEy, p[2]- rayEz, 
				    									n[0], n[1], n[2]);

				    // calculate intersection point with plane along the ray
				    // t = (p-e) * n / (v*n)
				    t = pen / MyMath.scalarProduct(rayVx, rayVy, rayVz, n[0], n[1], n[2]);

				    // already a closer intersection point? => next triangle
				    if (t >= minT)
				    	continue;

				    // the intersection point with the plane
				    ip[0] = rayEx + t * rayVx;
				    ip[1] = rayEy + t * rayVy;
				    ip[2] = rayEz + t * rayVz;

				    // no intersection point with the triangle? => next
				    // triangle
				    if (!triangleTest(ip, p1, p2, p3, a, ai))
				    	continue;

				    // from here: t < minT and triangle intersection
				    // I'm the winner until now!

				    minT = t;
				    minObjectsIndex = objectsNumber;
				    minIndexInT_Mesh = i;

				    // prepare everything for shading alternatives

				    // the intersection point
				    minIP[0] = ip[0];
				    minIP[1] = ip[1];
				    minIP[2] = ip[2];

				    switch (mesh.fgp) {
				    case 'f':
				    case 'F':
						// the normal is the surface normal 
						minN[0] = n[0];
						minN[1] = n[1]; 
						minN[2] = n[2];
						 
						// the material is the material of the first triangle point 
						int matIndex = mesh.verticesMat[mesh.triangles[minIndexInT_Mesh][0]];
						minMaterial = mesh.materials[matIndex]; 
						minMaterialN= mesh.materialsN[matIndex];
						
						break;
				    case 'g':
				    case 'G':
						// remember barycentric coordinates bu, bv, bw for shading
						bu = ai[0] / a;
						bv = ai[1] / a;
						bw = ai[2] / a;
	
						break;
				    case 'p':
				    case 'P':
						// the normal is barycentrically interpolated between
						// the three vertices
				    	bu = ai[0] / a;
						bv = ai[1] / a;
						bw = ai[2] / a;
						float nTemp[] = new float[3];
						int p1Idx = mesh.triangles[i][0];
						int p2Idx = mesh.triangles[i][1];
						int p3Idx = mesh.triangles[i][2];
						nTemp[0] = mesh.vertexNormals[ p1Idx ][0] * bu + 
								   mesh.vertexNormals[ p2Idx ][0] * bv + 
								   mesh.vertexNormals[ p3Idx ][0] * bw;
						nTemp[1] = mesh.vertexNormals[ p1Idx ][1] * bu + 
								   mesh.vertexNormals[ p2Idx ][1] * bv + 
								   mesh.vertexNormals[ p3Idx ][1] * bw;
						nTemp[2] = mesh.vertexNormals[ p1Idx ][2] * bu + 
								   mesh.vertexNormals[ p2Idx ][2] * bv + 
								   mesh.vertexNormals[ p3Idx ][2] * bw;
						
						minN = nTemp;
	
						// intermediate version
						// the material is not interpolated
						// matIndex =
						// mesh.verticesMat[mesh.triangles[minIndex][0]];
						// minMaterial = mesh.materials[matIndex];
						// minMaterialN = mesh.materialsN[matIndex];
	
						// the material is barycentrically interpolated between
						// the three vertex materials
						int matIndex0 = mesh.verticesMat[p1Idx];
						int matIndex1 = mesh.verticesMat[p2Idx];
						int matIndex2 = mesh.verticesMat[p3Idx];
						float materialTemp[] = new float[9];
						int materialNTemp;
						for (int k = 0; k < 9; k++) {
						    materialTemp[k] = mesh.materials[matIndex0][k] * bu +
						    				  mesh.materials[matIndex1][k] * bv +
						    				  mesh.materials[matIndex2][k] * bw;
						}
						minMaterial = materialTemp;
						materialNTemp = (int) (mesh.materialsN[matIndex0] * bu +
			    				  			   mesh.materialsN[matIndex1] * bv +
		    				  				   mesh.materialsN[matIndex2] * bw);
						minMaterialN = materialNTemp;
				    }
				}
		    }else
				continue; // return null;
		}

		// no intersection point found => return with no result
		if (minObjectsIndex == -1)
			return null;
		

		// viewing vector at intersection point
		// equals vector from IP to e
//		v[0] = rayEx - minIP[0];
//		v[1] = rayEy - minIP[1];
//		v[2] = rayEz - minIP[2];
		// or simply reverse rayV
		v[0] = -rayVx;
		v[1] = -rayVy;
		v[2] = -rayVz;
		normalize(v);
		
		// light vector at the intersection point
		// = vector from IP to ICenter(light point)
		l[0] = ICenter[0] - minIP[0];
		l[1] = ICenter[1] - minIP[1];;
		l[2] = ICenter[2] - minIP[2];;
		normalize(l);
		

		// decide which shading model will be applied

		// implicit: only phong shading available => shade=illuminate
		if (objects.get(minObjectsIndex) instanceof I_Sphere)
		    return phongIlluminate(minMaterial, minMaterialN, l, minN, v, Ia, Ids);
		
		// triangle mesh: flat, gouraud or phong shading according to file data
		else if (objects.get(minObjectsIndex).getHeader() == "TRIANGLE_MESH") {
		    mesh = ((T_Mesh) objects.get(minObjectsIndex));
		    switch (mesh.fgp) {
		    case 'f':
		    case 'F':
				// illumination can be calculated here
				// this is a variant between flat und phong shading
				//return phongIlluminate(minMaterial, minMaterialN, l, minN, v, Ia, Ids);
				float colorf_F[] = new float[3];
				colorf_F[0] = mesh.triangleColors[minIndexInT_Mesh][0];
				colorf_F[1] = mesh.triangleColors[minIndexInT_Mesh][1];
				colorf_F[2] = mesh.triangleColors[minIndexInT_Mesh][2];	
				return new Color(colorf_F[0], colorf_F[1], colorf_F[2]);
		    case 'g':
		    case 'G':
				// the color is barycentrically interpolated between the three
				// vertex colors
				float c_r =   mesh.vertexColors[mesh.triangles[minIndexInT_Mesh][0]] [0] * bu
							+ mesh.vertexColors[mesh.triangles[minIndexInT_Mesh][1]] [0] * bv
							+ mesh.vertexColors[mesh.triangles[minIndexInT_Mesh][2]] [0] * bw;
				float c_g =   mesh.vertexColors[mesh.triangles[minIndexInT_Mesh][0]] [1] * bu
							+ mesh.vertexColors[mesh.triangles[minIndexInT_Mesh][1]] [1] * bv
							+ mesh.vertexColors[mesh.triangles[minIndexInT_Mesh][2]] [1] * bw;
				float c_b =   mesh.vertexColors[mesh.triangles[minIndexInT_Mesh][0]] [2] * bu
							+ mesh.vertexColors[mesh.triangles[minIndexInT_Mesh][1]] [2] * bv
							+ mesh.vertexColors[mesh.triangles[minIndexInT_Mesh][2]] [2] * bw;	
				return new Color((c_r > 1.0f) ? 1.0f : c_r, 
						 (c_g > 1.0f) ? 1.0f : c_g, 
						 (c_b > 1.0f) ? 1.0f : c_b);
				
		    case 'p':
		    case 'P':
				// calculate the color per per pixel phong lightning
				return phongIlluminate(minMaterial, minMaterialN, l, minN, v, Ia, Ids);
				// return new Color(material[3], material[4], material[5]);
				// break;
		    }
		}
		
		return null;
//		// intermediate version
//		Random rd = new Random();
//		return new Color(rd.nextFloat(), rd.nextFloat(), rd.nextFloat());

	}
	
    // calculate phong illumination model with material parameters material and
    // materialN, light vector l, normal vector n, viewing vector v, ambient
    // light Ia, diffuse and specular light Ids
    // return value is a new Color object
    private Color phongIlluminate(float[] material, float materialN, float[] l, float[] n, float[] v, float[] Ia, float[] Ids) {
		float ir = 0, ig = 0, ib = 0; // reflected intensity, rgb channels
		float[] r = new float[3]; // reflection vector
		float ln, rv; // scalar products <l,n> and <r,v>// scalar products <l,n> and <r,v>
		
		// <l,n>
		ln = MyMath.scalarProduct(l, n);
		
		// ambient component, Ia*ra
		ir += Ia[0] * material[0];
		ig += Ia[1] * material[1];
		ib += Ia[2] * material[2];
	
		// diffuse component, Ids*rd*<l,n>
		if (ln > 0) {
		    ir += Ids[0]*material[3]*ln;
		    ig += Ids[1]*material[4]*ln;
		    ib += Ids[2]*material[5]*ln;
	
		    // reflection vector r=2*<l,n>*n-l
		    r[0] = 2*ln*n[0] - l[0];
		    r[1] = 2*ln*n[1] - l[1];
		    r[2] = 2*ln*n[2] - l[2];
		    normalize(r);
	
		    // <r,v>
		    rv = MyMath.scalarProduct(r, v);
	
		    // specular component, Ids*rs*<r,v>^n
		    if (rv > 0) {
				float rv_pow_n = (float) Math.pow(rv, materialN);
				ir += Ids[0] * material[6] * rv_pow_n;
				ig += Ids[1] * material[7] * rv_pow_n;
				ib += Ids[2] * material[8] * rv_pow_n;
		    }
		}
	
		// System.out.println(ir+" "+ig+" "+ib);
		return new Color((ir > 1.0f) ? 1.0f : ir, 
						 (ig > 1.0f) ? 1.0f : ig, 
						 (ib > 1.0f) ? 1.0f : ib);
	    }
	
    // vector normalization
    // CAUTION: vec is an in-/output parameter; the referenced object will be
    // altered!
    private float normalize(float[] vec) {
		float l = MyMath.getEuclideanNorm(vec);
		vec[0] = vec[0] / l;
		vec[1] = vec[1] / l;
		vec[2] = vec[2] / l;
		
		return l;
    }
    
    
    // calculate normalized face normal fn of the triangle p1, p2 and p3
    // the return value is the area of triangle
    // CAUTION: fn is an output parameter; the referenced object will be
    // altered!
    private float calculateN(float[] fn, float[] p1, float[] p2, float[] p3) {
		float ax, ay, az, bx, by, bz;
	
		// a = Vi2-Vi1, b = Vi3-Vi1
		// a = Vector from p1 to p2
		ax = p2[0] - p1[0];
		ay = p2[1] - p1[1];
		az = p2[2] - p1[2];
		
		// b = Vector from p1 to p3
		bx = p3[0] - p1[0];
		by = p3[1] - p1[1];
		bz = p3[2] - p1[2];		
	
		// n =a x b
		// nx = ay*bz - az*by
		fn[0] = ay*bz - az*by;
		// ny = -( ax*bz - az*bx )
		fn[1] = -( ax*bz - az*bx );
		// nz = ax*by - ay*bx
		fn[2] = ax*by - ay*bx;
	
		// normalize n, calculate and return area of triangle
		return normalize(fn) / 2;
    }

    // calculate triangle test
    // is p (the intersection point with the plane through p1, p2 and p3) inside
    // the triangle p1, p2 and p3?
    // the return value answers this question
    // a is an input parameter - the given area of the triangle p1, p2 and p3
    // ai will be computed to be the areas of the sub-triangles to allow to
    // compute barycentric coordinates of the intersection point p
    // ai[0] is associated with bu (p1p2p) across from p3
    // ai[1] is associated with bv (pp2p3) across from p1
    // ai[2] is associated with bw (p1pp3) across form p2
    // CAUTION: ai is an output parameter; the referenced object will be
    // altered!
    private boolean triangleTest(float[] p, float[] p1, float[] p2, float[] p3, float a, float ai[]) {
		//float tmp[] = new float[3];
		
		float[] p1ToP2 = MyMath.getVectorBetweenPoints(p1, p2);
		float[] p1ToP = MyMath.getVectorBetweenPoints(p1, p);
		float[] p2ToP3 = MyMath.getVectorBetweenPoints(p2, p3);
		float[] p2ToP = MyMath.getVectorBetweenPoints(p2, p);
		float[] p1ToP3 = MyMath.getVectorBetweenPoints(p1, p3);
		
		
		// Scalar product yields size of area of the parallelogram; /2 for the triangle
		ai[0] = MyMath.getEuclideanNorm(MyMath.crossProduct(p2ToP3, p2ToP)) / 2; 
		ai[1] = MyMath.getEuclideanNorm(MyMath.crossProduct(p1ToP3, p1ToP)) / 2; 
		ai[2] = MyMath.getEuclideanNorm(MyMath.crossProduct(p1ToP2, p1ToP)) / 2; 

		float epsilon = 1e-05f;
		if ( Math.abs( (ai[0]+ai[1]+ai[2]) - a) <= epsilon) 
		    return true;
	
		return false;
    }
    
    
    // calculate bounding box test
    // decides whether the ray s(t)=rayE+t*rayV intersects the axis aligned
    // bounding box of object -> return value true
    // six plane intersections with rectangle inside tests; if one succeeds
    // bounding box is hit
    private boolean bboxHit(RT_Object object, float rayEx, float rayEy, float rayEz, float rayVx, float rayVy, float rayVz) {
		
    	float t;
		float ip[] = new float[3];
	
		// front and back
		if (Math.abs(rayVz) > 1E-5) {
		    // front xy
		    t = (object.max[2] - rayEz) / rayVz;
	
		    ip[0] = rayEx + t * rayVx;
		    ip[1] = rayEy + t * rayVy;
	
		    if (ip[0] > object.min[0] && ip[0] < object.max[0] && 
		    		ip[1] > object.min[1] && ip[1] < object.max[1])
		    	return true;
	
		    // back xy
		    t = (object.min[2] - rayEz) / (-rayVz);
	
		    ip[0] = rayEx + t * rayVx;
		    ip[1] = rayEy + t * rayVy;
	
		    if (ip[0] > object.min[0] && ip[0] < object.max[0] && 
		    		ip[1] > object.min[1] && ip[1] < object.max[1] )
				return true;
		}
		
		// left and right
		if (Math.abs(rayVx) > 1E-5) {
			// left yz
		    t = (object.min[0] - rayEx) / rayVx;
	
		    ip[1] = rayEy + t * rayVy;
		    ip[2] = rayEz + t * rayVz;
	
		    if (ip[1] > object.min[1] && ip[1] < object.max[1] &&
		    		ip[2] > object.min[2] && ip[2] < object.max[2])
		    	return true;
	
		    // right yz
		    t = (object.max[0] - rayEx) / rayVx;
			
		    ip[1] = rayEy + t * rayVy;
		    ip[2] = rayEz + t * rayVz;
	
		    if (ip[1] > object.min[1] && ip[1] < object.max[1] &&
		    		ip[2] > object.min[2] && ip[2] < object.max[2])
		    	return true;
		}
		// top and bottom
		if (Math.abs(rayVy) > 1E-5) {
			// top xz
		    t = (object.max[1] - rayEy) / rayVy;
	
		    ip[0] = rayEx + t * rayVx;
		    ip[2] = rayEz + t * rayVz;
	
		    if (ip[0] > object.min[0] && ip[0] < object.max[0] &&
		    		ip[2] > object.min[2] && ip[2] < object.max[2])
		    	return true;
	
		    // bottom xz
		    t = (object.min[1] - rayEy) / rayVy;
			
		    ip[0] = rayEx + t * rayVx;
		    ip[2] = rayEz + t * rayVz;
	
		    if (ip[0] > object.min[0] && ip[0] < object.max[0] &&
		    		ip[2] > object.min[2] && ip[2] < object.max[2])
		    	return true;
		}
		return false;
    }
    
    // precalulation of triangle normals and triangle areas
    private void prepareMeshData() {
		RTFile scene;
	
		System.out.println("Vorverarbeitung 1 läuft");
	
		float[] p1, p2, p3;
	
		for (int objectsNumber = 0; objectsNumber < objects.size(); objectsNumber++) {
		    scene = objects.get(objectsNumber);
	
		    if (scene.getHeader() == "TRIANGLE_MESH") {
				T_Mesh mesh = (T_Mesh) scene;
		
				// init memory
				mesh.triangleNormals = new float[mesh.triangles.length][3];
				mesh.triangleAreas = new float[mesh.triangles.length];
		
				for (int i = 0; i < mesh.triangles.length; i++) {
					p1 = mesh.vertices[mesh.triangles[i][0]];
				    p2 = mesh.vertices[mesh.triangles[i][1]];
				    p3 = mesh.vertices[mesh.triangles[i][2]];
		
				    // calculate and store triangle normal n and triangle area a
				    mesh.triangleAreas[i] = calculateN(mesh.triangleNormals[i], p1, p2,p3);
				}
		    }
		}
		System.out.println("Vorverarbeitung 1 beendet");
    }
    
    // view dependend precalculation dependend on type of mesh shading
    // vertexNormals for phong and gouraud shading
    // vertexColors for gouraud shading
    // triangleColors for flat lighting
    private void precalculateMeshDataShading() {
		RTFile scene;
	
		System.out.println("Vorverarbeitung 2 läuft");
	
		float rayEx, rayEy, rayEz, rayVx, rayVy, rayVz;
		double rayVn;
		Color color;
		float x, y, z;
		float[] ip = new float[3];
		float[] n = new float[3];
		float[] l = new float[3];
		float[] v = new float[3];
		float[] material;
		float materialN;
		int matIndex;
	
		for (int objectsNumber = 0; objectsNumber < objects.size(); objectsNumber++) {
		    scene = objects.get(objectsNumber);
	
		    if (scene.getHeader() == "TRIANGLE_MESH") {
			T_Mesh mesh = (T_Mesh) scene;
	
			switch (mesh.fgp) {
			case 'f':
			case 'F':
			    // for flat-shading: initialize and calculate triangle
			    // colors
			    mesh.triangleColors = new float[mesh.triangles.length][3];
	
			    rayEx = 0.0f;
			    rayEy = 0.0f;
			    rayEz = 0.0f;
	
			    // loop over all triangles
			    for (int i = 0; i < mesh.triangles.length; i++) {
					// the intersection point is the first vertex of the
					// triangle
					ip = mesh.vertices[mesh.triangles[i][0]];
		
					// the material is the material of the first triangle
					// point
					matIndex = mesh.verticesMat[mesh.triangles[i][0]];
					material = mesh.materials[matIndex];
					materialN = mesh.materialsN[matIndex];
		
					// x, y, z: view coordinates are intersection point
					x = ip[0];
					y = ip[1];
					z = ip[2];
		
					// ray vector
					rayVx = x - rayEx;
					rayVy = y - rayEy;
					rayVz = z - rayEz;
		
					// fetch precalculated face normal
					n = mesh.triangleNormals[i];
		
					rayVn = rayVx * n[0] + rayVy * n[1] + rayVz * n[2];
		
					// backface? => next triangle
					if (rayVn >= 0)
					    continue;
		
					// light vector at the intersection point
					l[0] = ICenter[0] - ip[0];
					l[1] = ICenter[1] - ip[1];
					l[2] = ICenter[2] - ip[2];
					normalize(l);
		
					// viewing vector at intersection point
					v[0] = -rayVx;
					v[1] = -rayVy;
					v[2] = -rayVz;
					normalize(v);
		
					// illuminate
					color = phongIlluminate(material, materialN, l, n, v, Ia, Ids);
		
					// write color to triangle
					mesh.triangleColors[i][0] = (float) ( color.getRed() / 255.0 );
					mesh.triangleColors[i][1] = (float) ( color.getGreen() / 255.0 );
					mesh.triangleColors[i][2] = (float) ( color.getBlue() / 255.0 );
			    }
			    break;
	
			case 'p':
			case 'P':
			case 'g':
			case 'G':
			    // initialize and calculate averaged vertex normals
			    mesh.vertexNormals = new float[mesh.vertices.length][3];
	
			    // loop over all vertices to initialize
			    for (int j = 0; j < mesh.vertices.length; j++)
					for (int k = 0; k < 3; k++)
					    mesh.vertexNormals[j][k] = 0.0f;
	
			    // loop over all faces to contribute
			    for (int i = 0; i < mesh.triangles.length; i++)
					for (int j = 0; j < 3; j++) // Iterate over points in triangle
					    for (int k = 0; k < 3; k++) // Iterate over x,y,z
					    	mesh.vertexNormals[mesh.triangles[i][j]][k] += mesh.triangleNormals[i][k];
	
			    // loop over all vertices to normalize
			    for (int j = 0; j < mesh.vertices.length; j++) {
			    	normalize(mesh.vertexNormals[j]);
			    }
	
			    // these are all preparations for phong shading
			    if (mesh.fgp == 'p' || mesh.fgp == 'P')
			    	break;
	
			    // for gouraud-shading: initialize and calculate vertex
			    // colors
			    mesh.vertexColors = new float[mesh.vertices.length][3]; // 3 for r,g,b as float values
	
			    rayEx = 0.0f;
			    rayEy = 0.0f;
			    rayEz = 0.0f;
	
			    // loop over all vertices
			    for (int i = 0; i < mesh.vertices.length; i++) {
					// the intersection point is the vertex
					ip = mesh.vertices[i];
		
					// the material is the material of the vertex
					matIndex = mesh.verticesMat[i];
					material = mesh.materials[matIndex];
					materialN = mesh.materialsN[matIndex];
		
					// x, y, z: view coordinates are intersection point
					x = ip[0];
					y = ip[1];
					z = ip[2];
		
					// ray vector
					rayVx = x - rayEx;
					rayVy = y - rayEy;
					rayVz = z - rayEz;
		
					// fetch precalculated vertex normal
					n = mesh.vertexNormals[i];
		
					rayVn = rayVx * n[0] + rayVy * n[1] + rayVz * n[2];
		
					// backface? => next vertex
					if (rayVn >= 0)
					    continue;
		
					// light vector at the intersection point
					l[0] = ICenter[0] - ip[0];
					l[1] = ICenter[1] - ip[1];
					l[2] = ICenter[2] - ip[2];
					normalize(l);
		
					// viewing vector at intersection point
					v[0] = -rayVx;
					v[1] = -rayVy;
					v[2] = -rayVz;
					normalize(v);
		
					// illuminate
					color = phongIlluminate(material, materialN, l, n, v, Ia, Ids);
		
					// write color to vertex
					mesh.vertexColors[i][0] = color.getRed() / 255.0f;
					mesh.vertexColors[i][1] = color.getGreen() / 255.0f;
					mesh.vertexColors[i][2] = color.getBlue() / 255.0f;
			    }
			}
		    }
		}
		System.out.println("Vorverarbeitung 2 beendet");
    }



    

	public static void main(String[] args) {
		Raytracer rt = new Raytracer();

		rt.doRayTrace();
	}
}
