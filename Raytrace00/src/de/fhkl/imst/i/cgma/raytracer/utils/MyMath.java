package de.fhkl.imst.i.cgma.raytracer.utils;

public class MyMath {

	private static final double EPSILON = 1e-7;

	public static float getEuclideanNorm(float x, float y, float z) {
		return (float) Math.sqrt(scalarProduct(x, y, z, x, y, z));
	}
	
	public static float getEuclideanNorm(float[] vec) {
		return getEuclideanNorm(vec[0], vec[1], vec[2]);
	}
	
	public static float scalarProduct(float[] v1, float[] v2) {
		return scalarProduct(v1[0], v1[1], v1[2],
							v2[0], v2[1], v2[2]);
	}

	public static float scalarProduct(float x1, float y1, float z1,
			float x2, float y2, float z2) {
		return x1*x2 + y1*y2 + z1*z2;
	}

	public static boolean almostEqualsToZero(double a) {
		return almostEquals(a, 0.0);
	}
	
	public static boolean almostEquals(double a, double b) {
		return Math.abs(a-b) < EPSILON;
	}
	
	public static float[] getVectorBetweenPoints(float[] fromPoint, float[] toPoint)
	{
		float[] vector = new float[3];

		vector[0] = toPoint[0] - fromPoint[0];
		vector[1] = toPoint[1] - fromPoint[1];
		vector[2] = toPoint[2] - fromPoint[2];
		
		return vector;
	}
	
	public static float[] crossProduct(float[] a, float[] b) {
		return crossProduct(a[0], a[1], a[2], b[0], b[1], b[2]);
	}
	public static float[] crossProduct(float ax, float ay, float az,
			float bx, float by, float bz) {
		float[] n = new float[3];
		
		// n =a x b
		// nx = ay*bz - az*by
		n[0] = ay*bz - az*by;
		// ny = -( ax*bz - az*bx )
		n[1] = -( ax*bz - az*bx );
		// nz = ax*by - ay*bx
		n[2] = ax*by - ay*bx;
		
		return n;
	}

}
